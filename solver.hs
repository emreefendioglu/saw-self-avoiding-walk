-- O anki adım ile birlikte matrisi tekrar düzenler
resetMtrx s (x,y) mtrx m n = [ [ if x == x' && y == y' then s else ((mtrx!!y')!!x') | x' <- [0..(m-1)] ] | y' <- [0..(n-1)]]

-- Belirli başlangıç ve bitiş notaları için bütün kareleri dolaşarak
-- çizilebilecek yolu bulur ve mxn boyutlarında bir matris olarak döndürür
walker (x, y) (x', y') m n =
    walker' (x, y) (x',y') m n [ [ 0 | x' <- [0..m]] | y' <- [0..n] ] 1 1
    where 
        walker' (x, y) (x', y') m n mtrx step try =
            if step >= m*n && x == x' && y == y' -- Çözüm bulundu
            then resetMtrx step (x,y) mtrx m n
            else if length pts == 0 || ((length pts) - try) < 0
            then []
            else
                let _mtrx = resetMtrx step (x,y) mtrx m n
                in
                    let c = walker' (pts!!((length pts) - try)) (x', y') m n _mtrx (step+1) 1
                    in
                        if length c == 0
                        then walker' (x, y) (x', y') m n mtrx step (try+1)
                        else c
            where pts = [ p | p <- [ ( x + fst s, y + snd s ) | s <- [(-1,0), (0,-1), (1,0), (0,1)] ], fst p >= 0 && snd p >= 0, fst p < m && snd p < n, (mtrx!!(snd p))!!(fst p) == 0 ]

-- Yolu bulur ve matrisi anlaşılabilecek şekilde formatlayarak konsola yazdırır
visual (x, y) (x', y') m n = 
    let result = walker (x, y) (x', y') m n
    in
        putStr (vWalker 0 result)
    where 
        vWalker s result =
            if length result == 0
            then "Sonuç yok!\n"
            else if s == m*n
            then "\n\n"
            else (if (mod s n) == 0 then "\n|" else "")++(if c<=9 then " " else "")++(show c)++"|"++(vWalker (s+1) result)
            where c = (result!!(div s n))!!(mod s m)